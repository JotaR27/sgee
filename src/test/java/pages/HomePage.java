package pages;

import javax.xml.xpath.XPath;

import net.serenitybdd.core.Serenity;
import org.openqa.selenium.support.FindBy;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

//@DefaultUrl("http://afa02ad4e64334d92b3e8bd8f249c28a-2139825710.us-east-1.elb.amazonaws.com/autentication/signin?return=%2Fauthorization%2Fentity")

public class HomePage extends PageObject {

    @FindBy(xpath = "/html/body/app-root/app-auth-layout/mat-sidenav-container/mat-sidenav-content/div/app-signin/div/div[2]/form/div[1]/mat-form-field/div/div[1]/div/mat-select/div/div[1]/span")
    WebElementFacade btnTipoIdentificacion;

    @FindBy(xpath = "//*[@name='username']")
    WebElementFacade txtNumeroIdentificacion;

    @FindBy(xpath = "//*[@name='password']")
    WebElementFacade txtContrasena;

    @FindBy(xpath = "//*[@class='mat-focus-indicator btn-size-EE btn-blue mat-button mat-button-base']")
    WebElementFacade btnIngresar;

    @FindBy(xpath = "//*[@class='mat-focus-indicator topbar-button-right mr-16 img-button mat-icon-button mat-button-base']")
    WebElementFacade btnSalir;

    @FindBy(xpath = "//*[@class='alert-text icon-rojo']")
    WebElementFacade lblClaveOUsuarioErrado;

    @FindBy(xpath = "//*[@id='mat-select-0-panel']/mat-option[1]/span")
    WebElementFacade cmbCedulaCiudadania;

    @FindBy(xpath = "//*[@id='mat-select-0-panel']/mat-option[2]/span")
    WebElementFacade cmbCedulaExtranjeria;

    @FindBy(xpath = "//*[@id='mat-select-0-panel']/mat-option[3]/span")
    WebElementFacade cmbNit;

    @FindBy(xpath = "//*[@class='mat-dialog-actions']/button")
    WebElementFacade btnAceptar;


    public void Login(String tipodoc, String usuario, String contrasena) {

        Serenity.takeScreenshot();
        btnTipoIdentificacion.click();
        String cedulaciudadania = cmbCedulaCiudadania.getText();
        String cedulaExtranjeria = cmbCedulaExtranjeria.getText();
        String nit = cmbNit.getText();
        if (cedulaciudadania.contentEquals(tipodoc)) {
            cmbCedulaCiudadania.click();
        } else if (cedulaExtranjeria.contentEquals(tipodoc)) {
			cmbCedulaExtranjeria.click();
        } else if (nit.contentEquals(tipodoc)) {
			cmbNit.click();
        }
        Serenity.takeScreenshot();
        txtNumeroIdentificacion.sendKeys(usuario);
        Serenity.takeScreenshot();
        txtContrasena.sendKeys(contrasena);
        Serenity.takeScreenshot();
        btnIngresar.click();
        Serenity.takeScreenshot();

    }

    public void validacionIngreso() {

        if (btnSalir.isVisible()) {
            System.out.println("El Usuario ingreso correctamente");
			Serenity.takeScreenshot();
        } else if (lblClaveOUsuarioErrado.isVisible()){
            String mensaje = lblClaveOUsuarioErrado.getText();
            System.out.println(mensaje);
            btnAceptar.click();
			Serenity.takeScreenshot();
        }
    }
}
