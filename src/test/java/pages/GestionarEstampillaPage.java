package pages;

import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.FindBy;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.List;

public class GestionarEstampillaPage extends PageObject {


    @FindBy(xpath = "//*[@id='rightside-content-hold']/div/app-welcome/div/div/div[5]/div/app-card-menu/div/div[2]/button")
    WebElementFacade btnIrAEstampillas;

    @FindBy(xpath = "//*[@id='rightside-content-hold']/div/app-definition-list/div[1]/div/button")
    WebElementFacade btnRegistrarEstampilla;

    @FindBy(xpath = "//*[@id='mat-input-6']")
    WebElementFacade txtNombreEstampilla;

    @FindBy(xpath = "//*[@id='mat-input-7']")
    WebElementFacade txtNombreActoJuridico;

    @FindBy(xpath = "//*[@id='mat-select-value-9']/span")
    WebElementFacade lblNombreActoJuridico;

    @FindBy(xpath = "//*[@id='mat-select-8-panel']/mat-option")
    List<WebElementFacade> listNombreActorJuridico;

    @FindBy(xpath = "//*[@id='mat-input-8']")
    WebElementFacade txtResponsableDelRecaudo;

    @FindBy(xpath = "//*[@id='mat-input-9']")
    WebElementFacade txtBaseGrabableTarifa;

    @FindBy(xpath = "//*[@id='mat-select-8']")
    WebElementFacade lblEntidadTerritorial;

    @FindBy(xpath = "//*[@id='mat-select-8-panel']/mat-option/span")
    List<WebElementFacade> listEntidadTerritorial;

    @FindBy(xpath = "//*[@id='cdk-step-content-0-0']/app-definition-form/form/div/div[6]/div/mat-form-field/div/div[1]/div[2]/mat-datepicker-toggle/button")
    WebElementFacade btnCalendario;

    @FindBy(xpath = "//*[@id='mat-input-9']")
    WebElementFacade txtFechaDeVigencia;

    @FindBy(xpath = "//*[@id='cdk-step-content-0-0']/app-definition-form/form/mat-grid-list[1]/div/mat-grid-tile[1]/div/div/a")
    WebElementFacade btnGuardarBorrador;

    @FindBy(xpath = "//*[@id='cdk-step-content-0-0']/app-definition-form/form/mat-grid-list[2]/div/mat-grid-tile[2]/div/div/button")
    WebElementFacade btnSiguiente;

    @FindBy(xpath = "//*[@id='mat-dialog-0']/app-alerts/div/div[2]/mat-dialog-actions/button[2]")
    WebElementFacade btnConfirmar;

    @FindBy(xpath = "//*[@id='mat-dialog-1']/app-alerts/div/div[2]/mat-dialog-actions/button")
    WebElementFacade btnAceptarEstampillaTemporalmente;


    //---------------- Agregar hecho generador ------------------------//

    @FindBy(xpath = "//*[@id='mat-select-10']")
    WebElementFacade lblActoDocumento;

    @FindBy(xpath = "//*[@id='mat-select-10-panel']/mat-option")
    List<WebElementFacade> listActoDocumento;

    @FindBy(xpath = "//*[@id='mat-select-16']")
    WebElementFacade lblEntidad;

    @FindBy(xpath = "//*[@id='mat-select-16-panel']/mat-option")
    List<WebElementFacade> listEntidad;

    @FindBy(xpath = "//*[@id='mat-select-20']")
    WebElementFacade lblTramite;

    @FindBy(xpath = "//*[@id='mat-select-20-panel']/mat-option")
    List<WebElementFacade> listTramite;

    @FindBy(xpath = "//*[@id='mat-select-12']")
    WebElementFacade lblSubTramite;

    @FindBy(xpath = "//*[@id='mat-select-12-panel']/mat-option")
    List<WebElementFacade> listSubTramite;

    @FindBy(xpath = "//*[@id='mat-select-18']")
    WebElementFacade lblTipoContrato;

    @FindBy(xpath = "//*[@id='mat-select-18-panel']/mat-option")
    List<WebElementFacade> listTipoContrato;

    @FindBy(xpath = "//*[@id='mat-select-22']")
    WebElementFacade lblTipoOrganizacion;

    @FindBy(xpath = "//*[@id='mat-select-22-panel']/mat-option")
    List<WebElementFacade> listTipoOrganizacion;

    @FindBy(xpath = "//*[@id='mat-select-14']")
    WebElementFacade lblFuenteRecursos;

    @FindBy(xpath = "//*[@id='mat-select-14-panel']/mat-option")
    List<WebElementFacade> listFuenteRecursos;

    //---------------------------------- Agregr regla hecho generador -----------------------------//

    @FindBy(xpath = "//*[@id='mat-select-24']")
    WebElementFacade lblBaseLiquidacion;

    @FindBy(xpath = "//*[@id='mat-select-24-panel']/mat-option")
    List<WebElementFacade> listBaseLiquidacion;

    @FindBy(xpath = "//*[@id='mat-select-28']")
    WebElementFacade lblOperador;

    @FindBy(xpath = "//*[@id='mat-select-28-panel']/mat-option")
    List<WebElementFacade> listOperador;

    @FindBy(xpath = "//*[@id='mat-input-10']")
    WebElementFacade txtTarifa;

    @FindBy(xpath = "//*[@id='mat-select-26']")
    WebElementFacade lblUnidadTarifa;

    @FindBy(xpath = "//*[@id='mat-select-26-panel']/mat-option")
    List<WebElementFacade> listUnidadTarifa;

    @FindBy(xpath = "//*[@id='mat-radio-3']/label/span[1]")
    WebElementFacade rbReglaRedondeNo;



    @FindBy(xpath = "//*[@id='cdk-step-content-0-1']/app-generating-fact/form/div[3]/button")
    WebElementFacade btnAgregarhecho;



    @FindBy(xpath = "//*[@id='mat-dialog-2']/app-alerts/div/div[2]/mat-dialog-actions/button[2]")
    WebElementFacade btnAceptarHechoGenerador;

    @FindBy(xpath = "//*[@id='cdk-step-content-0-1']/app-generating-fact/div[2]/a")
    WebElementFacade btnGuardarBorradorHechoGenerador;

    @FindBy(xpath = "//*[@id='mat-dialog-3']/app-alerts/div/div[2]/mat-dialog-actions/button")
    WebElementFacade btnAceptarEstampilla2;

    @FindBy(xpath = "//*[@id='mat-dialog-4']/app-alerts/div/div[2]/mat-dialog-actions/button")
    WebElementFacade btnAceptarEstampilla3;

    @FindBy(xpath = "//*[@id='cdk-step-content-0-1']/app-generating-fact/div[3]/button[2]")
    WebElementFacade btnSiguiente2;


    //--------------- Crear exclusion a una estampilla ------------------//

    @FindBy(xpath = "//*[@id='mat-radio-2']/label/span[1]/span[1]")
    WebElementFacade rbSi;

    @FindBy(xpath = "//*[@id='mat-radio-6']/label/span[1]")
    WebElementFacade rbNo;

    @FindBy(xpath = "//*[@id='cdk-step-content-0-2']/app-exclusion-form/div[3]/a")
    WebElementFacade btnGuardarBorradorExclusion;

    @FindBy(xpath = "//*[@id='mat-dialog-5']/app-alerts/div/div[2]/mat-dialog-actions/button")
    WebElementFacade btnAceptarEstampillaTemporalmente3;

    @FindBy(xpath = "//*[@id='cdk-step-content-0-2']/app-exclusion-form/div[4]/button[2]")
    WebElementFacade btnRegistrarEstampillaExclusion;

    @FindBy(xpath = "//*[@id='mat-dialog-6']/app-alerts/div/div[2]/mat-dialog-actions/button[2]")
    WebElementFacade btnSiConfirmar;

    @FindBy(xpath = "//*[@id='mat-dialog-7']/app-alerts/div/div[2]/mat-dialog-actions/button")
    WebElementFacade btnAceptarFinal;


    //----------------  Consultar estampilla -------------------------//

    @FindBy(xpath = "//*[@name='stampName']")
    WebElementFacade txtNombreEstampillaConsulta;

    @FindBy(xpath = "//*[@id='cdk-accordion-child-1']/div/form/div[2]/button")
    WebElementFacade btnConsultarEstampilla;

    @FindBy(xpath = "//*[@id='rightside-content-hold']/div/app-definition-list/mat-grid-list/div/mat-grid-tile[1]/div/button/span[1]")
    WebElementFacade btnExportarDocumento;

    String nombreEstampilla="";

    Robot robot = new Robot();

    public GestionarEstampillaPage() throws AWTException {
    }

    public void leerLista(List<WebElementFacade> lista,String datoABuscar){
        for (int i=0; i<lista.size(); i++) {
            //System.out.println("Esta es la lista: "+lista.get(i).getText());
            if (lista.get(i).getText().equalsIgnoreCase(datoABuscar)) {
                lista.get(i).click();
                break;
            }
        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }



    public void gestionarEstampilla() throws InterruptedException {

        String nombre = "Emisión América Upaep";
        double numer = Math.random() * 999 + 1;
        int numero = (int) Math.round(numer);

        nombreEstampilla = nombre+numero;


        Thread.sleep(2000);


        btnIrAEstampillas.click();


        Thread.sleep(2000);


        btnRegistrarEstampilla.click();


        Thread.sleep(2000);

        //JavascriptExecutor js = (JavascriptExecutor) getDriver();
        //js.executeScript("arguments[0].scrollIntoView();", txtFechaDeVigencia);

        txtNombreEstampilla.sendKeys(nombreEstampilla);

        //lblNombreActoJuridico.click();

        Thread.sleep(2000);

        //Selecciona la opcion
        //leerLista(listNombreActorJuridico,"Entidad123");
        txtNombreActoJuridico.sendKeys("Ordenanza 1 de enero de 2022");

        txtResponsableDelRecaudo.sendKeys("Secretaria de Hacienda");
        //txtBaseGrabableTarifa.sendKeys("15%");

        lblEntidadTerritorial.click();

        Thread.sleep(2000);


        //Selecciona una opcion
        leerLista(listEntidadTerritorial,"Gobernación del Cauca");

        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("window.scrollBy(0,1000)");

        txtFechaDeVigencia.click();

        btnCalendario.click();

        Thread.sleep(2000);


        robot.keyPress(KeyEvent.VK_ENTER);



        Serenity.takeScreenshot();


        Thread.sleep(2000);

        btnGuardarBorrador.click();

        Thread.sleep(2000);

        btnConfirmar.click();

        Thread.sleep(2000);

        btnAceptarEstampillaTemporalmente.click();

        Thread.sleep(2000);


        btnSiguiente.click();

    }

    public void agregarHechoGenerador() throws InterruptedException {
        Thread.sleep(2000);

        lblActoDocumento.click();
        Thread.sleep(2000);
        leerLista(listActoDocumento,"Contratos");
        Thread.sleep(2000);

        lblEntidad.click();
        Thread.sleep(2000);
        leerLista(listEntidad,"Territorial");
        Thread.sleep(2000);

        lblTramite.click();
        Thread.sleep(2000);
        leerLista(listTramite,"Prórroga");
        Thread.sleep(2000);

        lblSubTramite.click();
        Thread.sleep(2000);
        leerLista(listSubTramite,"Fiducia para el manejo de anticipos");
        Thread.sleep(2000);

        lblTipoContrato.click();
        Thread.sleep(2000);
        leerLista(listTipoContrato,"Prestación de servicios");
        Thread.sleep(2000);

        lblTipoOrganizacion.click();
        Thread.sleep(2000);
        leerLista(listTipoOrganizacion,"Corporación");
        Thread.sleep(2000);

        lblFuenteRecursos.click();
        Thread.sleep(2000);
        leerLista(listFuenteRecursos,"Recursos de la Nación");
        Thread.sleep(2000);

        Serenity.takeScreenshot();

        //------- Agregar regla hecho generador ------------------//

        lblBaseLiquidacion.click();
        Thread.sleep(2000);
        leerLista(listBaseLiquidacion,"Valor del contrato");
        Thread.sleep(2000);

        lblOperador.click();
        Thread.sleep(2000);
        leerLista(listOperador,"Mayor");
        Thread.sleep(2000);

        txtTarifa.sendKeys("10");

        lblUnidadTarifa.click();
        Thread.sleep(2000);
        leerLista(listUnidadTarifa,"%");
        Thread.sleep(2000);

        //JavascriptExecutor js = (JavascriptExecutor) getDriver();
        //js.executeScript("arguments[0].scrollIntoView();", rbReglaRedondeNo);

        rbReglaRedondeNo.click();

        //------------------------------------------------//

        btnAgregarhecho.click();

        Thread.sleep(2000);

        btnAceptarHechoGenerador.click();

        Thread.sleep(2000);

        btnAceptarEstampilla2.click();

        Thread.sleep(2000);

        btnGuardarBorradorHechoGenerador.click();

        Thread.sleep(2000);

        btnAceptarEstampilla3.click();

        Thread.sleep(2000);

        btnSiguiente2.click();

    }

    public void crearExclusion() throws InterruptedException {
        Thread.sleep(2000);

        rbNo.click();

        Thread.sleep(2000);

        btnGuardarBorradorExclusion.click();

        Thread.sleep(2000);

        btnAceptarEstampillaTemporalmente3.click();

        Thread.sleep(2000);

        btnRegistrarEstampillaExclusion.click();

        Thread.sleep(2000);

        btnSiConfirmar.click();

        Thread.sleep(2000);

        btnAceptarFinal.click();

    }


    public void consultarEstampilla() throws InterruptedException {
        Thread.sleep(2000);

        txtNombreEstampillaConsulta.sendKeys(nombreEstampilla);
        Thread.sleep(2000);
        btnConsultarEstampilla.click();

        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("arguments[0].scrollIntoView();", btnExportarDocumento);

        Serenity.takeScreenshot();

        Thread.sleep(2000);

    }
}
