package pages;

import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.apache.xalan.transformer.XalanProperties;
import org.openqa.selenium.support.FindBy;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.List;

public class GestionarHechoGeneradorPage extends PageObject {

    @FindBy(xpath = "//*[@id='rightside-content-hold']/div/app-welcome/div/div/div[2]/div/app-card-menu/div/div[2]/button")
    WebElementFacade btnIrAHechosGeneradores;

    @FindBy(xpath = "//*[@id='rightside-content-hold']/div/app-facts/div/div[1]/button")
    WebElementFacade btnCrearHechoGenerador;

    @FindBy(xpath = "//*[@id='mat-select-50']")
    WebElementFacade lblNombreEntidadTerritorial;

    @FindBy(xpath = "//*[@id='mat-select-50-panel']/mat-option")
    List<WebElementFacade> listNombreEntidadTerritorial;

    @FindBy(xpath = "//*[@id='mat-select-58']")
    WebElementFacade lblNombreEntidadDescentralizada;

    @FindBy(xpath = "//*[@id='mat-select-72-panel']/mat-option")
    List<WebElementFacade> listNombreEntidadDescentralizada;

    @FindBy(xpath = "//*[@id='mat-select-76']")
    WebElementFacade lblOrganizacionSuscribeContrato;

    @FindBy(xpath = "//*[@id='mat-select-76-panel']/mat-option")
    List<WebElementFacade> listOrganizacionSuscribeContrato;

    @FindBy(xpath = "//*[@id='mat-select-66']")
    WebElementFacade lblActoDocumento;

    @FindBy(xpath = "//*[@id='mat-select-66-panel']/mat-option")
    List<WebElementFacade> listActoDocumento;

    @FindBy(xpath = "//*[@id='mat-input-27']")
    WebElementFacade txtCodigohechoGenerador;

    @FindBy(xpath = "//*[@id='mat-input-50']")
    WebElementFacade txtValorHechoGenerador;

    @FindBy(xpath = "//*[@id='mat-select-112']")
    WebElementFacade lblTipoIdentificacionSujeto;

    @FindBy(xpath = "//*[@id='mat-select-112-panel']/mat-option")
    List<WebElementFacade> listTipoIdentificacionSujeto;

    @FindBy(xpath = "//*[@id='mat-input-48']")
    WebElementFacade txtNumeroIdentificacion;

    @FindBy(xpath = "//*[@id='mat-input-31']")
    WebElementFacade txtNombresYApellidosORazonSocial;

    @FindBy(xpath = "//*[@id='rightside-content-hold']/div/app-facts-form/form/div[1]/div[1]/div[4]/mat-form-field/div/div[1]/div[2]/mat-datepicker-toggle/button/span[1]/svg")
    WebElementFacade btnFechaInicio;

    @FindBy(xpath = "//*[@id='rightside-content-hold']/div/app-facts-form/form/div[1]/div[2]/div[4]/mat-form-field/div/div[1]/div[2]/mat-datepicker-toggle/button/span[1]/svg")
    WebElementFacade btnFechaFin;

    @FindBy(xpath = "//*[@id='mat-select-70']")
    WebElementFacade lblTramite;

    @FindBy(xpath = "//*[@id='mat-select-70-panel']/mat-option")
    List<WebElementFacade> listTramite;

    @FindBy(xpath = "//*[@id='mat-select-74']")
    WebElementFacade lblSubTramite;

    @FindBy(xpath = "//*[@id='mat-select-74-panel']/mat-option")
    List<WebElementFacade> listSubTramite;

    @FindBy(xpath = "//*[@id='rightside-content-hold']/div/app-facts-form/form/div[2]/button[2]")
    WebElementFacade btnRegistrarHechoGenerador;

    Robot robot = new Robot();

    public GestionarHechoGeneradorPage() throws AWTException {
    }


    public void leerLista(List<WebElementFacade> lista,String datoABuscar){
        for (int i=0; i<lista.size(); i++) {
            System.out.println("Esta es la lista: "+lista.get(i).getText());
            if (lista.get(i).getText().equalsIgnoreCase(datoABuscar)) {
                lista.get(i).click();
                break;
            }
        }
        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    public void registrarHechoGenerador(){
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        btnIrAHechosGeneradores.click();

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        btnCrearHechoGenerador.click();

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        lblNombreEntidadTerritorial.click();
        leerLista(listNombreEntidadTerritorial,"Gobernación del Cauca");

        lblNombreEntidadDescentralizada.click();
        leerLista(listNombreEntidadDescentralizada,"Dirección Territorial de Salud de Caldas                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ");

        lblOrganizacionSuscribeContrato.click();
        leerLista(listOrganizacionSuscribeContrato,"Liga deportiva");

        lblActoDocumento.click();
        leerLista(listActoDocumento,"Convenios");

        txtCodigohechoGenerador.sendKeys("123456");

        txtValorHechoGenerador.sendKeys("300000");

        lblTipoIdentificacionSujeto.click();
        leerLista(listTipoIdentificacionSujeto,"Natural");

        txtNumeroIdentificacion.sendKeys("79545880");

        txtNombresYApellidosORazonSocial.sendKeys("Diego Maradona");

        btnFechaInicio.click();

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        robot.keyPress(KeyEvent.VK_ENTER);

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        btnFechaFin.click();

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        robot.keyPress(KeyEvent.VK_ENTER);

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        lblTramite.click();
        leerLista(listTramite,"No Aplica");

        lblSubTramite.click();
        leerLista(listSubTramite,"No Aplica");


        Serenity.takeScreenshot();

        btnRegistrarHechoGenerador.click();


    }





}
