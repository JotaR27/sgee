package pages;

import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.FindBy;


public class GestionarEntidadesPage extends PageObject {

    String nombreRamdomAutomation ="";

    @FindBy(xpath = "//*[@id='rightside-content-hold']/div/app-welcome/div/div/div[1]/div/app-card-menu/div/div[2]/button")
    WebElementFacade btnIrAEntidades;

    @FindBy(xpath = "//*[@id='rightside-content-hold']/div/app-territorial-entities/mat-grid-list/div/mat-grid-tile[3]/div/div/button")
    WebElementFacade btnCrearEntidad;

    @FindBy(xpath = "//*[@name='nombreentidad']")
    WebElementFacade txtNombreEntidad;

    @FindBy(xpath = "//*[@name='numeroidentificacione']")
    WebElementFacade txtNumeroDeIdentificacion;

    @FindBy(xpath = "//*[@name='tipoentidad']")
    WebElementFacade txtTipoEntidad;

    @FindBy(xpath = "//*[@name='ubicacionentidad']")
    WebElementFacade txtUbicacionEntidad;

    @FindBy(xpath = "//*[@id='rightside-content-hold']/div/app-formcompany/div/mat-card-content/mat-card/mat-card-content/form/div[3]/button")
    WebElementFacade btnCrear;

    @FindBy(xpath = "//*[@class='mat-focus-indicator btn-size-EE btn-blue mat-button mat-button-base']")
    WebElementFacade btnSiConfirmar;

    @FindBy(xpath = "//*[@class='mat-dialog-actions']/button")
    WebElementFacade btnAceptar;

    @FindBy(xpath = "/html/body/app-root/app-admin-layout/div/mat-sidenav-container/mat-sidenav-content/div[1]/div/div/app-territorial-entities/div/div[1]/mat-form-field[1]/div/div[1]/div/input")
    WebElementFacade txtBuscar;




    public void gestionarEntidad(){

        JavascriptExecutor js = (JavascriptExecutor) getDriver();


        Serenity.takeScreenshot();
        btnIrAEntidades.click();

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        js.executeScript("arguments[0].scrollIntoView();",btnCrearEntidad );
        Serenity.takeScreenshot();

        btnCrearEntidad.click();

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        String Automation = "Automation";
        double numer = Math.random() * 9999 + 1;
        double iden = Math.random() * 999999 + 1;
        int numero = (int) Math.round(numer);
        int Identificacion = (int) Math.round(iden);

        nombreRamdomAutomation = Automation+numero;
        String Identi = String.valueOf(Identificacion);

        txtNombreEntidad.sendKeys(nombreRamdomAutomation);
        txtNumeroDeIdentificacion.sendKeys(Identi);
        txtTipoEntidad.sendKeys("Publica");
        txtUbicacionEntidad.sendKeys("Bogota");

        Serenity.takeScreenshot();
        btnCrear.click();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        Serenity.takeScreenshot();

        btnSiConfirmar.click();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        Serenity.takeScreenshot();

        btnAceptar.click();

    }

    public void buscarEntidadCreada(){

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        txtBuscar.sendKeys(nombreRamdomAutomation);
        Serenity.takeScreenshot();
    }


}
