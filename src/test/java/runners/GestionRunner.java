package runners;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        features = "src/test/resources/feature/GestionSGEE.feature",
        glue = "",
        monochrome= true,
        snippets = SnippetType.CAMELCASE,
        tags = {"@Gestionar_Estampilla"}
)

public class GestionRunner {
}
