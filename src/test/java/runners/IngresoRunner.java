package runners;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
		features = "src/test/resources/feature/",
		glue = "",
		monochrome= true,
		snippets = SnippetType.CAMELCASE,
		tags = {"@Ingreso_SGEE"}
		)
public class IngresoRunner {

}
