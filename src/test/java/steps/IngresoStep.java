package steps;

import net.thucydides.core.annotations.Step;
import pages.GestionarEntidadesPage;
import pages.HomePage;

public class IngresoStep {
	
	HomePage homePage;
	GestionarEntidadesPage gestionarEntidadesPage;
	
	@Step
	public void homeOpen() {
		homePage.open();
	}
	
	@Step
	public void ingreso(String tipodoc, String usuario, String contrasena) {
		homePage.Login(tipodoc, usuario,contrasena);
	}

	@Step
	public void validacionIngreso() {
		homePage.validacionIngreso();
	}

	@Step
	public void crearEntidad(){
		gestionarEntidadesPage.gestionarEntidad();
	}

	@Step
	public void buscarEntidadCreada(){
		gestionarEntidadesPage.buscarEntidadCreada();
	}

}
