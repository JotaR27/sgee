package steps;

import net.thucydides.core.annotations.Step;
import pages.GestionarEntidadesPage;
import pages.GestionarEstampillaPage;
import pages.HomePage;

public class GestionStep {

    HomePage homePage;
    GestionarEntidadesPage gestionarEntidadesPage;
    GestionarEstampillaPage gestionarEstampillaPage;


    @Step
    public void gestionarEntidad(){
        gestionarEntidadesPage.gestionarEntidad();
    }

    @Step
    public void buscarEntidadCreada(){
        gestionarEntidadesPage.buscarEntidadCreada();
    }

    @Step
    public void gestionarEstampilla() throws InterruptedException {
        gestionarEstampillaPage.gestionarEstampilla();
        gestionarEstampillaPage.agregarHechoGenerador();
        gestionarEstampillaPage.crearExclusion();
    }

    @Step
    public void buscarEstampillaCreada() throws InterruptedException {
        gestionarEstampillaPage.consultarEstampilla();
    }

}
