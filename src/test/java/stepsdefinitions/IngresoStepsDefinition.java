package stepsdefinitions;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Utilidades.DataDrivenExcel;
import Utilidades.Excel;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;
import steps.GestionStep;
import steps.IngresoStep;

public class IngresoStepsDefinition {
	
	
	@Steps
	IngresoStep ingresoStep;
	@Steps
	GestionStep gestionStep;
	
	DataDrivenExcel dataDriverExcel = new DataDrivenExcel();
	Map<String, String> data = new HashMap<String, String>();
	
	@Given("^Esta en la pagina de inicio(\\d+)$")
	public void estaEnLaPaginaDeInicio(int row, DataTable dataExcel) throws Exception {
		List<Map<String, String>> dataFeature = dataExcel.asMaps(String.class, String.class);
		Excel excel = new Excel(dataFeature.get(0).get("Ruta Excel"), dataFeature.get(0).get("Pestana"), true, row);
		data = dataDriverExcel.leerExcel(excel);
		ingresoStep.homeOpen();
	}

	@When("^Ingresamos al modulo de ir a estampillas$")
	public void ingresamosAlModuloDeIrAEstampillas() throws Exception {
		ingresoStep.ingreso(data.get("TipoDoc"), data.get("Usuario"), data.get("Contrasena"));
		gestionStep.gestionarEstampilla();
	}


	@When("^Ingresa al portal SGEE$")
	public void ingresaARealizarLaTransferencia() throws Exception {
		ingresoStep.ingreso(data.get("TipoDoc"), data.get("Usuario"), data.get("Contrasena"));
		ingresoStep.crearEntidad();
		ingresoStep.buscarEntidadCreada();
	}


	@Then("^Validacion de ingreso$")
	public void validacionIngreso() throws Exception {
	    // Write code here that turns the phrase above into concrete actions
		//ingresoStep.validacionIngreso();
		gestionStep.buscarEstampillaCreada();
	}

}
